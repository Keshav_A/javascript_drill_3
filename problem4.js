// Q4 Group users based on their Programming language mentioned in their designation.
export default function groupingUsers(users_log, groupBy) {
  let grouping = false;
  if (typeof users_log !== "object") {
    console.log("Function need object as an argument");
    return;
  } else if (Object.keys(users_log).length === 0) {
    console.log("Empty object");
    return;
  }
  if (!groupBy) {
    console.log("Provide arguments to groupBy");
    return;
  }
  if (typeof(groupBy)!== "object"){
    console.log("Provide arguments to groupBy in form of Array");
    return;
  }
  

  const group = {};

  for (let user in users_log) {
    for (let value = 0; value<groupBy.length;value++ ) {

      if (
        users_log[user].desgination
          .toLowerCase()
          .indexOf(groupBy[value].toLowerCase()) > -1
      ) {
        grouping = true;

        if (Object.keys(group).includes(groupBy[value])) {
          group[groupBy[value]].push(user);
        }
        else{
            group[groupBy[value]]=[user];
        }
      }
    }
  }
  console.log(group);
  if (grouping === false) {
    console.log("Grouping not successful");
    return;
  }
}
