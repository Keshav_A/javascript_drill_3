// Q1 Find all users who are interested in playing video games.
export default function gamers(users_log){
    let search = false;
    if (typeof(users_log)!=="object"){
        console.log("Function need object as an argument");
        return;
    }
    else if (Object.keys(users_log).length===0){
            console.log("Empty object");
            return;
    }
    const users = Object.keys(users_log);
    for (let user_index in users){
        const user_interest = users_log[users[user_index]].interests
        for(let interest_index = 0;interest_index < user_interest.length; interest_index++){
            if (user_interest[interest_index].toLowerCase().indexOf("video games")>-1){
                search = true;
                console.log(users[user_index]);
            }
        }
    }
    if (search === false){
        console.log("Did not find any users interested in Video games");
        return;
    }
}

