# JavaScript Drill 3
## Users log Drill


### Procedure
- The data.js file contains the details of people.
- The problem*.js files contain functions to solve the problem
- Navigate to test folder, and run the testProblem_*.js 
    ```
    node testProblem_*.js
    ```
    Replace * with relevent problem number.

### Problems:
- **1):** Find all users who are interested in playing video games.
- **2):** Find all users staying in Germany.
- **3):** Find all users with masters Degree.
- **4):** Group users based on their Programming language mentioned in their designation.

### Higher Order functions
Branch 'HOF' contains the problem*.js files where the problem are solved in in higher order functions.