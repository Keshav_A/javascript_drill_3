// Q2 Find all users staying in Germany.
export default function citizensOf(users_log, nationality){
    let search = false;
    if (typeof(users_log)!=="object"){
        console.log("Function need object as an argument");
        return;
    }
    else if (Object.keys(users_log).length===0){
            console.log("Empty object");
            return;
    }
    const users = Object.keys(users_log);
    for (let user in users){
        if (users_log[users[user]].nationality===nationality){
            search = true;
            console.log(users[user]);
        }
    }
    if (search === false){
        console.log("Did not find any users with given Nationality. Try checking your spelling.");
        return;
    }
}