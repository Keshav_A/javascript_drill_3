// Q3 Find all users with masters Degree.
export default function education_level(users_log, education){
    let search = false;
    if (typeof(users_log)!=="object"){
        console.log("Function need object as an argument");
        return;
    }
    else if (Object.keys(users_log).length===0)
    {
        console.log("Empty object");
        return;
    }
    const users = Object.keys(users_log);
    for (let user in users){
        if (users_log[users[user]].qualification.toLowerCase().indexOf(education.toLowerCase())>-1){
            search = true;
            console.log(users[user]);
        }
    }
    if (search === false){
        console.log("Did not find any users with given qualification. Try checking your spelling.");
        return;
    }
}